﻿using System;
using UnityEngine;
using DG.Tweening;

namespace Script.ButtonSystem
{
    public class Button : MonoBehaviour
    {
        public int index;
        public ButtonData data;
        private RectTransform _rectTransform;
        private Vector2 _position;

        public event Action<int> OnButtonPressed = delegate { };

        private void Start()
        {
            /*GetComponent<Button>().onClick.AddListener(() => OnButtonPressed(index));*/
            _rectTransform = GetComponent<RectTransform>();
        }
        
        public void RegisterListener(Action<int> listener) {
            OnButtonPressed += listener;
        }

        public void Initialized(int index)
        {
            this.index = index;
        }

        public void Move(int x, int y)
        {
            _position = new Vector2(x, y);
            _rectTransform.DOAnchorPos(_position, 0.5f).SetEase(Ease.InOutQuad);
            _rectTransform.DOScale(0.25f, 0.25f);
        }
    }
}