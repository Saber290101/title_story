﻿using UnityEngine;

public class ListNode<T>
{
    public T data;
    public ListNode<T> next;
    public ListNode<T> prev;

    public ListNode(T value)
    {
        data = value;
        next = null;
        prev = null;
    }
}

public class LinkedList<T>
{
    private ListNode<T> head;
    private ListNode<T> tail;
    private int count;

    public int Count { get { return count; } }

    public void Add(T value)
    {
        ListNode<T> newNode = new ListNode<T>(value);

        if (head == null)
        {
            head = newNode;
            tail = newNode;
        }
        else
        {
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode;
        }
        count++;
    }

    public T Get(int index)
    {
        if (index < 0 || index >= count)
        {
            Debug.LogError("Index out of range");
            return default(T);
        }

        ListNode<T> current = head;
        for (int i = 0; i < index; i++)
        {
            current = current.next;
        }
        return current.data;
    }

    public void RemoveAt(int index)
    {
        if (index < 0 || index >= count)
        {
            Debug.LogError("Index out of range");
            return;
        }

        ListNode<T> current = head;
        for (int i = 0; i < index; i++)
        {
            current = current.next;
        }

        if (current.prev != null)
        {
            current.prev.next = current.next;
        }
        else
        {
            head = current.next;
        }

        if (current.next != null)
        {
            current.next.prev = current.prev;
        }
        else
        {
            tail = current.prev;
        }

        count--;
    }
}