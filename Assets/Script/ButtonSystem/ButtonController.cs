﻿using System.Collections.Generic;
using UnityEngine;

namespace Script.ButtonSystem
{
    public class ButtonController : MonoBehaviour
    {
        private LinkedList<Button> _choseButtons = new(); 
        [SerializeField] private ButtonView view;
        private Button[] _buttonList;
        private Dictionary<int, List<int>> _endPosition = new(); 

        private void Awake()
        {
            ConnectView();
            InitializeEndPosition(); 
        }

        private void Start()
        {
            _buttonList = view.buttons;
        }

        void ConnectView()
        {
            foreach (Button button in view.buttons)
            {
                button.RegisterListener(OnEachButtonPressed);
            }
        }

        void OnEachButtonPressed(int index)
        {
            _choseButtons.Add(_buttonList[index]);
            
        }

        private void InitializeEndPosition()
        {
            for (int i = 0; i < 9; i++)
            {
                _endPosition.Add(i, new List<int> { -390 + (i * 220), -550 });
            }
        }
    }
}