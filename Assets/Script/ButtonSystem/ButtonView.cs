﻿using System;
using UnityEngine;

namespace Script.ButtonSystem
{
    public class ButtonView : MonoBehaviour
    {
        public Button[] buttons;

        private void Awake()
        {
            for (var i = 0; i < buttons.Length; i++)
            {
                buttons[i].Initialized(i);
            }
        }
    }
}