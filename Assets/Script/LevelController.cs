using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelController : MonoBehaviour
{
    private readonly Dictionary<string, List<int>> _itemDictionary = new Dictionary<string, List<int>>();
    private readonly Dictionary<string, List<int>> _itemPositions = new Dictionary<string, List<int>>();

    public List<RectTransform> RectTransforms = new List<RectTransform>(); 
    public ParticleSystem explosionParticle;

    public void Banhmy(int i)
    {
        UpdateItemQuantity("banh");
        UpdateRectTransformPosition("banh", i);
    }

    public void So(int i)
    {
        UpdateItemQuantity("So");
        UpdateRectTransformPosition("So", i);
    }

    /*public void AddList(RectTransform rectTransform)
    {
        // add vao list vi tri moi nhung chua duyet duoc
        // vi tri day de chay ham private void UpdateRectTransformPosition(string itemName, int i)
        RectTransforms.Add(rectTransform);
    }*/
    
    //fix cung hang 1 vi tri nhu duoi
    
    private void UpdateRectTransformPosition(string itemName, int i)
    {
        if (_itemPositions.ContainsKey(itemName) && i >= 0 && i < RectTransforms.Count)
        {
            List<int> positions = _itemPositions[itemName];
            if (positions.Count > 0)
            {
                Vector2 newPosition = Vector2.zero;
                switch (positions.Count)
                {
                    case 1:
                        newPosition = new Vector2(-390f, -550f);
                        break;
                    case 2:
                        newPosition = new Vector2(-280f, -550f);
                        break;
                    case 3:
                        newPosition = new Vector2(-165f, -550f);
                        break;
                    default:
                        Debug.LogWarning("No positions available for " + itemName);
                        break;
                }
                MoveRectTransformToPosition(RectTransforms[i], newPosition);
            }
            else
            {
                Debug.LogWarning("No positions available for " + itemName);
            }
        }
        else
        {
            Debug.LogWarning("Invalid index specified for RectTransforms list or " + itemName + " dictionary does not contain the key.");
        }
    }

    private void UpdateItemQuantity(string itemName)
    {
        if (_itemDictionary.ContainsKey(itemName))
        {
            _itemDictionary[itemName].Add(1);
            _itemPositions[itemName].Add(_itemDictionary[itemName].Count - 1);
            if (_itemDictionary[itemName].Count == 3)
            {
                Debug.Log("Merge for " + itemName);
                Invoke(nameof(ScaleGameObjectToZero), 0.5f);
            }
        }
        else
        {
            _itemDictionary[itemName] = new List<int> { 1 };
            _itemPositions[itemName] = new List<int> { 0 };
        }
        Debug.Log(itemName + " in dictionary: " + _itemDictionary.ContainsKey(itemName));
        Debug.Log("Positions of " + itemName + ": " + string.Join(", ", _itemPositions[itemName]));
    }

    private void MoveRectTransformToPosition(RectTransform rectTransform, Vector2 position)
    {
        //di chuyen game object vs scale no nho lai
        rectTransform.DOAnchorPos(position, 0.5f).SetEase(Ease.InOutQuad);
        rectTransform.DOScale(0.25f, 0.25f);
    }

    private void ScaleGameObjectToZero()
    {
        foreach (RectTransform rectTransform in RectTransforms)
        {
            rectTransform.DOScale(0.26f, 0.1f).SetEase(Ease.Linear).OnComplete(() =>
            {
                rectTransform.DOScale(0f, 0.25f).SetEase(Ease.InOutQuad);
                if (explosionParticle != null)
                {
                    explosionParticle.Play();
                }
            });
        }
    }
}
