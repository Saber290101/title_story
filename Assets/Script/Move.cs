using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Move : MonoBehaviour
{
    public static Move Instance;
    [SerializeField]
    private RectTransform targetRectTransform; // Reference to the target RectTransform

    [SerializeField]
    private float duration = 1f; // Duration of the movement animation

    private void Awake()
    {
        Instance = this;
    }

    public void MoveToTargetPosition()
    {
        // Kiểm tra xem targetRectTransform có tồn tại không
        if (targetRectTransform != null)
        {
            // Lấy vị trí của RectTransform
            Vector2 targetPosition = targetRectTransform.anchoredPosition;

            // Di chuyển GameObject đến vị trí RectTransform trong thời gian duration
            transform.DOLocalMove(targetPosition, duration);
            transform.DOScale(0.25f, duration);
            Debug.LogError(targetPosition);
            
        }
        else
        {
            Debug.LogError("Target RectTransform is not assigned.");
        }
    }
}