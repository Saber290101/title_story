using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public List<RectTransform> rectTransforms = new List<RectTransform>();

    // Move a RectTransform to a specific position
    public void MoveRectTransformToPosition(RectTransform rectTransform, Vector2 position)
    {
        rectTransform.anchoredPosition = position;
    }
}